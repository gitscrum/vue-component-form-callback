module.exports = {
  entry: './src/index.js',
  output: {
    path: __dirname + '/build/',
    publicPath: 'build/',
    filename: 'callback.js'
  },
  module: {
    loaders: [
      { test: /\.vue$/, loader: 'vue' },
      // process *.js files using babel-loader
      // the exclude pattern is important so that we don't
      // apply babel transform to all the dependencies!
      { test: /\.js$/, loader: 'babel', exclude: /node_modules/ }
    ]
  },
  // configure babel-loader.
  // this also applies to the JavaScript inside *.vue files
  babel: {
    presets: ['es2015'],
    plugins: ['transform-runtime']
  }
}
